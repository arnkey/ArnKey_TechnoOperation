﻿//#define DEMO

using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml.Linq;
using Tekla.Structures.Model;
using UI = Tekla.Structures.Model.UI;
using g = Tekla.Structures.Geometry3d;
using System.Diagnostics;
using System.IO;
using x = OfficeOpenXml;


namespace ArnKey_TechnoOperation
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

		#region FIELDS

        string lang = "";
        List<string> lang_names = new List<string>();

        private string LENGTH = "";
        private string MATERIAL = "";
        private string PART_POS = "";
        private string PROFILE = "";

        private string MENU_language = "";
        private string MENU_create = "";
        private string MENU_open = "";
        private string MENU_degree = "";
        private string MENU_save = "";
        private string MENU_tol = "";
        private string MENU_panel = "";
        private string MENU_rb_1 = "";
        private string MENU_rb_2 = "";

        private string ITEM_part_pos = "";
        private string ITEM_profile = "";
        private string ITEM_material = "";
        private string ITEM_number = "";
        private string ITEM_length = "";
        private string ITEM_chamfer_edge = "";
        private string ITEM_chamfer_end_face = "";
        private string ITEM_part_cutting = "";
        private string ITEM_holes = "";
        private string ITEM_bend = "";
        private string ITEM_bevel_X = "";
        private string ITEM_bevel_Y = "";
		private string ITEM_mill = "";

        private string S_degree = "°";
        private string S_number = "";
        private string S_name_table = "";
        private static int S_value_round = 0;
        private string S_diam = "";

        private string SYS_message_not_numbering = "";
        private string SYS_message_not_objects = "";

		#endregion

        void LoadConfig()
        {

            comboBox1.Items.Clear();
            lang_names.Clear();

            XElement cfg = XElement.Load("config.xml");
            lang = cfg.Element("language").Attribute("current").Value;

            foreach (XElement x in cfg.Elements("language"))
            {
                foreach (XElement x0 in x.Elements())
                {
                    lang_names.Add(x0.Value);
                    comboBox1.Items.Add(x0.Value);
                }
            }

            // attributes
            LENGTH = cfg.Element("default").Element("length").Value;
            MATERIAL = cfg.Element("default").Element("material").Value;
            PART_POS = cfg.Element("default").Element("part_number").Value;
            PROFILE = cfg.Element("default").Element("profile").Value;
            // others:
            S_degree = cfg.Element("default").Element("value_degree").Value;
            S_number = cfg.Element("default").Element("number").Element(lang).Value;
            SYS_message_not_numbering = cfg.Element("default").Element("not_number").Element(lang).Value;
            S_name_table = cfg.Element("default").Element("name_table_excel").Element(lang).Value;
            S_value_round = int.Parse(cfg.Element("default").Element("value_round").Value);
            SYS_message_not_objects = cfg.Element("default").Element("not_objects").Element(lang).Value;
            S_diam = cfg.Element("default").Element("value_diam").Value;
            // menu
            MENU_language = cfg.Element("default").Element("menu").Element("language").Element(lang).Value;
            MENU_create = cfg.Element("default").Element("menu").Element("create").Element(lang).Value;
            MENU_open = cfg.Element("default").Element("menu").Element("open").Element(lang).Value;
            MENU_degree = cfg.Element("default").Element("menu").Element("degree").Element(lang).Value;
            MENU_save = cfg.Element("default").Element("menu").Element("save").Element(lang).Value;
            MENU_tol = cfg.Element("default").Element("menu").Element("tol").Element(lang).Value;
            MENU_panel = cfg.Element("default").Element("menu").Element("panel").Element(lang).Value;
            MENU_rb_1 = cfg.Element("default").Element("menu").Element("rb_1").Element(lang).Value;
            MENU_rb_2 = cfg.Element("default").Element("menu").Element("rb_2").Element(lang).Value;
            // items
            ITEM_part_pos = cfg.Element("item").Element("part_pos").Element(lang).Value;
            ITEM_profile = cfg.Element("item").Element("profile").Element(lang).Value;
            ITEM_material = cfg.Element("item").Element("material").Element(lang).Value;
            ITEM_number = cfg.Element("item").Element("number").Element(lang).Value;
            ITEM_length = cfg.Element("item").Element("length").Element(lang).Value;
            ITEM_chamfer_edge = cfg.Element("item").Element("chamfer_edge").Element(lang).Value;
            ITEM_chamfer_end_face = cfg.Element("item").Element("chamfer_end_face").Element(lang).Value;
            ITEM_part_cutting = cfg.Element("item").Element("part_cutting").Element(lang).Value;
            ITEM_holes = cfg.Element("item").Element("holes").Element(lang).Value;
            ITEM_bend = cfg.Element("item").Element("bend").Element(lang).Value;
            ITEM_bevel_X = cfg.Element("item").Element("bevel_X").Element(lang).Value;
            ITEM_bevel_Y = cfg.Element("item").Element("bevel_Y").Element(lang).Value;
			ITEM_mill = cfg.Element("item").Element("mill").Element(lang).Value;
            //
            comboBox1.SelectedItem = cfg.Element("language").Element(lang).Value;
            // to controls:
            lbl_lang.Text = MENU_language;
            deg90.Text = MENU_degree;
            ixml.Text = MENU_save;
            open.Text = MENU_open;
            btn_info.Text = MENU_create;
            ch_tol.Text = MENU_tol;
            radioButton1.Text = MENU_rb_1;
            radioButton2.Text = MENU_rb_2;
            groupBox1.Text = MENU_panel;
        }

        Model model = new Model();

        List<string> PositionParts = new List<string>();
        XElement table = new XElement("list");

		public string GetPath(string filename)
        {
			//TODO: DEL IT
			if (!Directory.Exists (model.GetInfo ().ModelPath + @"\Reports\")) Directory.CreateDirectory (model.GetInfo ().ModelPath + @"\Reports\");
			// TODO: FIX IT:
			return Path.Combine(model.GetInfo().ModelPath + @"\Reports\", filename);
        }

		#region Date
        bool CheckDate()
        {
			// исправлено , теперь можно задать range даты
			DateTime dt = DateTime.Parse("27.05.2017");
			if (DateTime.Now <= dt) return true;
            return false;
        }
		#endregion

		#region фрезеровка
		//обработка поверхности с нулевой плотностью - является фрезеровкой
		// по "ТЗ"
		bool Mill(Part p)
		{
			#region surfacing
			ModelObjectEnumerator moe = p.GetChildren ();
			List<SurfaceTreatment> childs = new List<SurfaceTreatment> ();
			while (moe.MoveNext ()) {
				if (moe.Current is SurfaceTreatment)
					childs.Add (moe.Current as SurfaceTreatment);
			}
			//surf.Material.MaterialString == "Tiles"
			foreach (SurfaceTreatment surf in childs) {
				if (surf.Name == "SURFACING" && surf.Type == SurfaceTreatment.SurfaceTypeEnum.STEEL_FINISH && surf.Thickness == 0.3 && surf.Color == SurfaceTreatment.SurfaceColorEnum.RED)
					return true;
			}
			#endregion
			return false;
		}
		#endregion

        private void button1_Click(object sender, EventArgs e)
        {

			//Settings stg = new Settings ();
			//stg.ShowDialog ();

			//return;


			string dateLog = DateTime.Now.ToShortDateString ();
			string iLog = "";
			try {
#if DEMO
	            if (!CheckDate())
	            {
	                MessageBox.Show("Время демо-версии истекло!");
	                this.Close();
	            }
#endif
	            PositionParts.Clear();
	            table = new XElement("list");

	            UI.ModelObjectSelector mos = new UI.ModelObjectSelector();
				ModelObjectEnumerator moe = radioButton1.Checked == true ? mos.GetSelectedObjects() : new Model().GetModelObjectSelector().GetAllObjects();
			    List<Part> parts = new List<Part>();
				while(moe.MoveNext())
				{
					if (moe.Current is Part)
					{
						parts.Add(moe.Current as Part);
					}
				}
				if (parts.Count == 0)
		        {
		            MessageBox.Show(SYS_message_not_objects);
		            return;
		        }
				Sorting(ref parts);

			//while (moe.MoveNext ()) {
				foreach(Part p in parts) {
				try {
					//if (moe.Current is Part) {
						//Part p = moe.Current as Part;
						if (p.Select ()) {
							List<ark_Chamfer> chamfs = new List<ark_Chamfer> ();
							// check on "normal" pos
							Tuple<bool, string> infoPos = GetPartPos (p);
							// holes
							Dictionary<double, int> Infoholes = GetHole (p);
							// mill
							bool mill = Mill(p);

							// chamfer
							bool deformation = false; // arc type
							bool bChamfer = Chamfer (p, ref deformation, ref chamfs, ref iLog); // chamfer_end_face ( addicted of welds )
							// look at chamfers if: bool part is cplate && points = 3 && is ortho triangle
							bool bBools = Booleans (p, ref chamfs, ref mill); // chamfer edge
							// cut's plane, fittings
							Tuple<bool, List<double>, List<double>> bFitt = Fittings (p, deg90.Checked);
							// deformation
							bool deform = Deformation (p, deformation);

							double length = 0;
							GetLength (p, ref length);
							string material = "";
							GetMaterial (p, ref material);
							string profile = "";
							GetProfile (p, ref profile);


							string[] message = SYS_message_not_numbering.Split (new char[1] { ';' });
							if (!infoPos.Item1 && (MessageBox.Show (message [0] + Environment.NewLine + message [1], "", MessageBoxButtons.OKCancel) == DialogResult.Cancel)) {
								return;
							} else {
								if (PositionParts.Contains (infoPos.Item2)) {
									UpdateNum (infoPos.Item2);
								} else {
									NewItem (infoPos.Item2, material, profile, 1, length, Infoholes, deform, bChamfer, chamfs, bFitt.Item2, bFitt.Item3, bBools, mill);
									PositionParts.Add (infoPos.Item2);
								}
							}
						}
					//}
				} catch (Exception ex) {
					using (StreamWriter sw = File.AppendText ("log_" + dateLog + ".txt")) {
					    sw.WriteLine(iLog);
						sw.WriteLine("id= " + p.Identifier.ID.ToString());
						sw.WriteLine (ex.ToString ());
						sw.Flush ();
						sw.Close ();
					}
						MessageBox.Show("Critical error!\nPlease send log file to programmer!","");
						return;
				}
			}
		    name = "ODD_Flow_chart_of_the_workpiece_" + (DateTime.Now.ToShortDateString() + (ixml.Checked == true ? ".xml" : ".xlsx"));
            if (ixml.Checked)
				table.Save(GetPath(name));
            else
            {
                SaveEXCEL();
            }
			if (open.Checked) Process.Start(GetPath(name));
			} catch (Exception ex ) {
				using (StreamWriter sw = File.AppendText ("logbug" + dateLog + ".txt")) {
					sw.WriteLine (ex.ToString ());
					sw.Flush ();
					sw.Close ();
				}
				MessageBox.Show ("Operation with excel file was failed!\nMay be problem is: File opened in Excel.\nClose the Excel and try again.", "");
			}
		}

		#region EXCEL
        string name = "ODD_Flow_chart_of_the_workpiece_";
        void SaveEXCEL()
        {
			FileInfo newFile = new FileInfo(GetPath(name));
            if (newFile.Exists)
            {
                newFile.Delete(); 
				newFile = new FileInfo(GetPath(name));
            }

            using (x.ExcelPackage package = new x.ExcelPackage(newFile))
            {
                string[] letter = new string[13] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M" };

                x.ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(S_name_table);
                #region names
                worksheet.Cells[(letter[0] + "1")].Value = ITEM_part_pos;
                worksheet.Cells[(letter[1] + "1")].Value = ITEM_profile;
                worksheet.Cells[(letter[2] + "1")].Value = ITEM_material;
                worksheet.Cells[(letter[3] + "1")].Value = ITEM_number;
                worksheet.Cells[(letter[4] + "1")].Value = ITEM_length;
                worksheet.Cells[(letter[5] + "1")].Value = ITEM_part_cutting;
                worksheet.Cells[(letter[6] + "1")].Value = ITEM_bend;
                worksheet.Cells[(letter[7] + "1")].Value = ITEM_bevel_X;
                worksheet.Cells[(letter[8] + "1")].Value = ITEM_bevel_Y;
                worksheet.Cells[(letter[9] + "1")].Value = ITEM_holes;
                worksheet.Cells[(letter[10] + "1")].Value = ITEM_chamfer_end_face;
                worksheet.Cells[(letter[11] + "1")].Value = ITEM_chamfer_edge;
				worksheet.Cells[(letter[12] + "1")].Value = ITEM_mill;

                worksheet.Cells[letter[0] + "1:" + letter[letter.Length - 1] + "1"].Style.WrapText = true;
                worksheet.Cells[letter[0] + "1:" + letter[letter.Length - 1] + "1"].Style.Font.Bold = true;

                #endregion


                int k = 2;
                foreach(XElement x in table.Elements("item"))
                {
                    worksheet.Cells[(letter[0] + k.ToString())].Value = x.Attribute(ITEM_part_pos.Replace(" ", "_")).Value;
                    worksheet.Cells[(letter[1] + k.ToString())].Value = x.Element(ITEM_profile.Replace(" ", "_")).Value;
                    worksheet.Cells[(letter[2] + k.ToString())].Value = x.Element(ITEM_material.Replace(" ", "_")).Value;
                    worksheet.Cells[(letter[3] + k.ToString())].Value = int.Parse(x.Element(ITEM_number.Replace(" ", "_")).Value);
                    worksheet.Cells[(letter[4] + k.ToString())].Value = double.Parse(x.Element(ITEM_length.Replace(" ", "_")).Value);
                    worksheet.Cells[(letter[5] + k.ToString())].Value = x.Element(ITEM_part_cutting.Replace(" ", "_")).Value;
                    worksheet.Cells[(letter[6] + k.ToString())].Value = x.Element(ITEM_bend.Replace(" ", "_")).Value;
					worksheet.Cells[(letter[12] + k.ToString())].Value = x.Element(ITEM_mill.Replace(" ", "_")).Value;

                    int var1 = k;
                    foreach (XElement x0 in x.Elements(ITEM_bevel_X.Replace(" ", "_")))
                    {
                        worksheet.Cells[(letter[7] + var1.ToString())].Value = x0.Value;
                        var1++;
                    }
                    var1 = k;
                    foreach (XElement x0 in x.Elements(ITEM_bevel_Y.Replace(" ", "_")))
                    {
                        worksheet.Cells[(letter[8] + var1.ToString())].Value = x0.Value;
                        var1++;
                    }

                    //worksheet.Cells[(letter[9] + k.ToString())].Value = x.Element(ITEM_holes.Replace(" ", "_")).Value;
                    worksheet.Cells[(letter[10] + k.ToString())].Value = x.Element(ITEM_chamfer_end_face.Replace(" ", "_")).Value;

                    int var2 = k;
                    foreach (XElement x0 in x.Elements(ITEM_chamfer_edge.Replace(" ", "_")))
                    {
                        worksheet.Cells[(letter[11] + var2.ToString())].Value = x0.Value;
                        var2++;
                    }
                    int var3 = k;
                    foreach (XElement x0 in x.Elements(ITEM_holes.Replace(" ", "_")))
                    {
                        worksheet.Cells[(letter[9] + var3.ToString())].Value = x0.Value;
                        var3++;
                    }

                    if (var1 - k > 1 || var2 - k > 1 || var3 - k > 1)
                    {
                        k += Math.Max(Math.Max(var1 - k, var2 - k), var3 - k);
                    }
                    else k++;
                }
                worksheet.Cells[letter[0] + "1:" + letter[letter.Length - 1] + (k - 1)].Style.Border.Bottom.Style = x.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[letter[0] + "1:" + letter[letter.Length - 1] + (k - 1)].Style.Border.Top.Style = x.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[letter[0] + "1:" + letter[letter.Length - 1] + (k - 1)].Style.Border.Left.Style = x.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[letter[0] + "1:" + letter[letter.Length - 1] + (k - 1)].Style.Border.Right.Style = x.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[letter[0] + "1:" + letter[letter.Length - 1] + (k - 1)].Style.HorizontalAlignment = x.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[letter[0] + "1:" + letter[letter.Length - 1] + (k - 1)].Style.VerticalAlignment = x.Style.ExcelVerticalAlignment.Center;
               // worksheet.Cells[letter[0] + "1:" + letter[letter.Length - 1] + (k - 1)].Style.ShrinkToFit = true;
                package.Save();
            }
        }
		#endregion
		void Sorting(ref List<Part> parts)
		{
			parts.Sort ((el, e2) => {
				string sl = "";
				el.GetReportProperty ("PART_POS", ref sl);
				string se = "";
				e2.GetReportProperty ("PART_POS", ref se);
				var res = sl.CompareTo (se);
				return res;
			});
		}

        bool Deformation(Part p, bool deformation_chamf)
        {
            if (tp(p) == TypePart.beam || tp(p) == TypePart.polybeam)
            {
                DeformingData dd = p.DeformingData;
                if (dd.Cambering > 0 || dd.Angle > 0 || dd.Angle2 > 0) return true;
                else return deformation_chamf;
            }
            return false;
        }

        Tuple<bool, List<double>, List<double>> Fittings(Part p, bool is90degree = false)
        {
			//Model TeklaModel = new Model();
			//WorkPlaneHandler workPlaneHandler = TeklaModel.GetWorkPlaneHandler();
			//TransformationPlane originalTransformationPlane = workPlaneHandler.GetCurrentTransformationPlane();
			//TransformationPlane tp = new TransformationPlane (p.GetCoordinateSystem ());
			//workPlaneHandler.SetCurrentTransformationPlane (tp);

            List<double> gradX = new List<double>();
            List<double> gradY = new List<double>();
            ModelObjectEnumerator moe = p.GetBooleans();
            int iCount = 0;
            while (moe.MoveNext())
            {
                Plane plane;

                if (moe.Current is CutPlane) plane = (moe.Current as CutPlane).Plane;
                else if (moe.Current is Fitting) plane = (moe.Current as Fitting).Plane;
                else continue;



                g.Point p1 = p.GetReferenceLine(false)[0] as g.Point;
                g.Point p2 = p.GetReferenceLine(false)[1] as g.Point;

                g.Vector pVector = new g.Vector(p2 - p1);

                double angle_X = Math.Round(pVector.GetAngleBetween(plane.AxisX) / Math.PI * 180, 0);
                double angle_Y = Math.Round(pVector.GetAngleBetween(plane.AxisY) / Math.PI * 180, 0);

				bool flag = true;

				if (!is90degree && (angle_X == 90 && angle_Y == 90)) {
					flag = false;
				}

				if (flag) {
					iCount++;
					gradX.Add (Math.Round (pVector.GetAngleBetween (plane.AxisX) / Math.PI * 180, 2));
					gradY.Add (Math.Round (pVector.GetAngleBetween (plane.AxisY) / Math.PI * 180, 2));
				}
            }
			//workPlaneHandler.SetCurrentTransformationPlane (originalTransformationPlane);
            return iCount > 0 ? new Tuple<bool, List<double>, List<double>>(true, gradX, gradY) : new Tuple<bool, List<double>, List<double>>(false, null, null);
        }


        /// <summary>
        /// chamfer edge
        /// </summary>
        public class ark_Chamfer
        {
            /*public enum type
            {
                edge,
                end_face
            }*/
            public double X = 0;
            public double Y = 0;
           // public type Type = type.edge;

            //public ark_Chamfer(type type, double x, double y)
            public ark_Chamfer(double x, double y)
            {
                X = Math.Round(x, S_value_round);
                Y = Math.Round(y, S_value_round);
                //Type = type;
            }
        }


        double GetLengthVector(g.Vector v)
        {
            double x = Math.Pow(v.X, 2);
            double y = Math.Pow(v.Y, 2);
            double z = Math.Pow(v.Z, 2);
            return (Math.Pow(x + y + z, 0.5));
        }

        bool Booleans(Part p, ref List<ark_Chamfer> chamf, ref bool mill, bool isChamfMinusBool = true)
        {
            ModelObjectEnumerator moe = p.GetBooleans();
            int iCount = 0;
            while(moe.MoveNext())
            {
                if (moe.Current is BooleanPart)
                {
                    // bool_parts
                    g.OBB obbPart = CreateOrientedBoundingBox(p);
                    g.OBB obbBool = CreateOrientedBoundingBox((moe.Current as BooleanPart).OperativePart);
                    if (obbPart.Intersects(obbBool)) iCount++;
                    // bool part is chamfer or mill?
                    if ((moe.Current as BooleanPart).OperativePart is ContourPlate)
                    {
                        ContourPlate cp = (moe.Current as BooleanPart).OperativePart as ContourPlate;

						List<g.Point> points = new List<g.Point> ();
						foreach (ContourPoint point in cp.Contour.ContourPoints)
							points.Add (new g.Point (point.X, point.Y, point.Z));
						
						if (points.Count == 3) {
							#region BOOLS: chamfs

							g.Vector AB = new g.Vector (points [2] - points [1]);
							g.Vector BC = new g.Vector (points [1] - points [0]);
							g.Vector CA = new g.Vector (points [0] - points [2]);

							double angle_A = Math.Round (AB.GetAngleBetween (CA) / Math.PI * 180, 0);
							double angle_B = Math.Round (AB.GetAngleBetween (BC) / Math.PI * 180, 0);
							double angle_C = Math.Round (BC.GetAngleBetween (CA) / Math.PI * 180, 0);

							if (angle_A == 90 || angle_B == 90 || angle_C == 90) {
								if (isChamfMinusBool) {
									double length_x = 0;
									double length_y = 0;

									if (angle_A == 90) {
										length_x = GetLengthVector (AB);
										length_y = GetLengthVector (CA);
									}
									if (angle_B == 90) {
										length_x = GetLengthVector (AB);
										length_y = GetLengthVector (BC);
									}
									if (angle_C == 90) {
										length_x = GetLengthVector (BC);
										length_y = GetLengthVector (CA);
									}
									chamf.Add (new ark_Chamfer (length_x, length_y));
									iCount--;
								}
							}
							#endregion
						} else if (points.Count == 4 && (cp.Name.ToLower().Contains("mill") || cp.Name.ToLower().Contains("фрез"))) {
							#region BOOLS: mill

							g.Vector p1 = new g.Vector (points [0] - points [1]);
							g.Vector p2 = new g.Vector (points [1] - points [2]);
							g.Vector p3 = new g.Vector (points [2] - points [3]);
							g.Vector p4 = new g.Vector (points [3] - points [0]);

							// 90 ? radian * pi * 180 / 90
							bool angle_1 = Math.Round (p1.GetAngleBetween (p4) / Math.PI * 2, 0) == 1 ? true : false;
							bool angle_2 = Math.Round (p2.GetAngleBetween (p1) / Math.PI * 2, 0) == 1 ? true : false;
							bool angle_3 = Math.Round (p3.GetAngleBetween (p2) / Math.PI * 2, 0) == 1 ? true : false;
							bool angle_4 = Math.Round (p4.GetAngleBetween (p3) / Math.PI * 2, 0) == 1 ? true : false;

							// if boolean part is rectangle and parallel or orthogonal to part then mill = true
							if (angle_1 && angle_2 && angle_3 && angle_4) {
								g.GeometricPlane gp = new g.GeometricPlane(points[0], p1, p2);
								g.Vector vPart = new g.Vector((p.GetReferenceLine(false)[0] as g.Point) - (p.GetReferenceLine(false)[1] as g.Point));
								if (g.Parallel.VectorToPlane(vPart, gp) == true ) mill = true;

								bool o1 = Math.Round (vPart.GetAngleBetween (p4) / Math.PI * 2, 0) == 1 ? true : false;
								bool o2 = Math.Round (vPart.GetAngleBetween (p1) / Math.PI * 2, 0) == 1 ? true : false;
								bool o3 = Math.Round (vPart.GetAngleBetween (p2) / Math.PI * 2, 0) == 1 ? true : false;
								bool o4 = Math.Round (vPart.GetAngleBetween (p3) / Math.PI * 2, 0) == 1 ? true : false;

								if (o1 || o2 || o3 || o4) mill = true;
							}

							#endregion
						}
                        else iCount++;
                    } else iCount++;
                }
                if (moe.Current is EdgeChamfer)
                {
                    chamf.Add(new ark_Chamfer(((EdgeChamfer)moe.Current).Chamfer.X, ((EdgeChamfer)moe.Current).Chamfer.Y));
                }
            }
            return iCount > 0;
        }


        #region OOBB
        private g.Point CalculateCenterPoint(g.Point min, g.Point max)
        {
            double x = min.X + ((max.X - min.X) / 2);
            double y = min.Y + ((max.Y - min.Y) / 2);
            double z = min.Z + ((max.Z - min.Z) / 2);

            return new g.Point(x, y, z);
        }

        private g.OBB CreateOrientedBoundingBox(Part beam, double DX = -0.001, double DY = -0.001, double DZ = -0.001)
        {
            g.Point dx = new g.Point(DX, DX, DX); // если детали сборки соприкасаются, то выдает как false , поэтому вводим зазор, управляемый пользователем, но не менее 0.01 мм

            g.OBB obb = null;

            if (beam != null)
            {
                Model TeklaModel = new Model();
                WorkPlaneHandler workPlaneHandler = TeklaModel.GetWorkPlaneHandler();
                TransformationPlane originalTransformationPlane = workPlaneHandler.GetCurrentTransformationPlane();

                Solid solid = beam.GetSolid();
                g.Point minPointInCurrentPlane = solid.MinimumPoint;
                g.Point maxPointInCurrentPlane = solid.MaximumPoint;

                g.Point centerPoint = CalculateCenterPoint(minPointInCurrentPlane, maxPointInCurrentPlane);

                g.CoordinateSystem coordSys = beam.GetCoordinateSystem();
                TransformationPlane localTransformationPlane = new TransformationPlane(coordSys);
                workPlaneHandler.SetCurrentTransformationPlane(localTransformationPlane);

                solid = beam.GetSolid();
                g.Point minPoint = solid.MinimumPoint - dx;
                g.Point maxPoint = solid.MaximumPoint + dx;

                double extent0 = (maxPoint.X - minPoint.X) / 2;
                double extent1 = (maxPoint.Y - minPoint.Y) / 2;
                double extent2 = (maxPoint.Z - minPoint.Z) / 2;

                workPlaneHandler.SetCurrentTransformationPlane(originalTransformationPlane);

                obb = new g.OBB(centerPoint, coordSys.AxisX, coordSys.AxisY,
                                coordSys.AxisX.Cross(coordSys.AxisY), extent0, extent1, extent2);
            }

            return obb;
        }
        #endregion

		#region Chamfs
		bool Chamfer(Part p, ref bool deformation, ref List<ark_Chamfer> chamfers, ref string iLog)
        {
			int iLogCount = 0; // for log
            bool flag = false;
            // standard chamfers
            #region standard chamfers of plate or polybeam
            ArrayList pp = new ArrayList();
			iLog += "\ninit pp";
            if (tp(p) == TypePart.plate)
            {
                pp = (p as ContourPlate).Contour.ContourPoints;
            }
            else if (tp(p) == TypePart.polybeam)
            {
                pp = (p as PolyBeam).Contour.ContourPoints;
            }
			iLog += "\ncount pp: " + pp.Count.ToString();
			List<ark_Chamfer> cha = GetChamferFromCP(pp, ref deformation);
			if (cha.Count > 0)
			{
				flag = true;
				foreach (ark_Chamfer ch in cha)
		        {
					iLog += "\nforeach: + " + (iLogCount++).ToString();
		            chamfers.Add(ch);
		        }
			}
			iLog += "\nchamfers count: " + chamfers.Count.ToString();
            #endregion
            // chamfers from weld
            ModelObjectEnumerator welds = p.GetWelds();
			iLogCount = 0;
			if (welds.GetSize() > 0) {
				welds = p.GetWelds();
				while (welds.MoveNext ()) {
                    try
                    {
                        iLog += "\nwhile: + " + (iLogCount++).ToString();
                        Weld w = welds.Current as Weld;
                        w.Select();
                        // init data of preparation
                        // angle
                        double angle = (w.AngleAbove > 0 ? w.AngleAbove : w.AngleBelow > 0 ? w.AngleBelow : 0);
                        // type
                        bool isPrepareted = GetPreparetedWeld(w.TypeAbove); // addict of type of weld
                        bool main = p.Identifier.ID == w.MainObject.Identifier.ID ? true : false;
                        // if preparation == main||secondary && angle > 0 && type of weld = value which can to preparate
                        if (isPrepareted && angle > 0 && Preparation(w.Preparation, main))
                            flag = true;
                    }
                    catch (Exception ex) { }
				}
			}
			iLog += "\nreturn : " + flag.ToString ();
            return flag;
        }
        #endregion

        bool Preparation(BaseWeld.WeldPreparationTypeEnum w, bool main = true)
        {
            if (main && (w == BaseWeld.WeldPreparationTypeEnum.PREPARATION_AUTO || w == BaseWeld.WeldPreparationTypeEnum.PREPARATION_MAIN) ||
               (!main && (w == BaseWeld.WeldPreparationTypeEnum.PREPARATION_AUTO || w == BaseWeld.WeldPreparationTypeEnum.PREPARATION_SECONDARY)))
                return true;
            else
                return false;
        }

        bool GetPreparetedWeld(BaseWeld.WeldTypeEnum wte)
        {
            if (wte == BaseWeld.WeldTypeEnum.STEEP_FLANKED_BEVEL_GROOVE_SINGLE_V_BUTT ||
                wte == BaseWeld.WeldTypeEnum.STEEP_FLANKED_BEVEL_GROOVE_SINGLE_BEVEL_BUTT ||
                wte == BaseWeld.WeldTypeEnum.WELD_TYPE_BEVEL_GROOVE_SINGLE_V_BUTT ||
                wte == BaseWeld.WeldTypeEnum.WELD_TYPE_BEVEL_GROOVE_SINGLE_BEVEL_BUTT ||
                wte == BaseWeld.WeldTypeEnum.WELD_TYPE_SQUARE_GROOVE_SQUARE_BUTT ||
                wte == BaseWeld.WeldTypeEnum.WELD_TYPE_SINGLE_V_BUTT_WITH_BROAD_ROOT_FACE ||
                wte == BaseWeld.WeldTypeEnum.WELD_TYPE_SINGLE_BEVEL_BUTT_WITH_BROAD_ROOT_FACE ||
                wte == BaseWeld.WeldTypeEnum.WELD_TYPE_U_GROOVE_SINGLE_U_BUTT ||
                wte == BaseWeld.WeldTypeEnum.WELD_TYPE_J_GROOVE_J_BUTT) return true;
            else return false;
        }

        /// <summary>
        /// standard chamfers
        /// </summary>
        /// <param name="cps"></param>
        /// <returns></returns>
        List<ark_Chamfer> GetChamferFromCP(ArrayList cps, ref bool deform)
        {
            List<ark_Chamfer> ch = new List<ark_Chamfer>();
            foreach (ContourPoint cpoint in cps)
            {
                if ((cpoint.Chamfer.Type == Tekla.Structures.Model.Chamfer.ChamferTypeEnum.CHAMFER_ARC || cpoint.Chamfer.Type == Tekla.Structures.Model.Chamfer.ChamferTypeEnum.CHAMFER_ARC_POINT))
                    deform = true; // arc type
                if (cpoint.Chamfer.Type != Tekla.Structures.Model.Chamfer.ChamferTypeEnum.CHAMFER_NONE &&
                    (cpoint.Chamfer.X > 0 && cpoint.Chamfer.Y > 0) && 
                    (cpoint.Chamfer.Type != Tekla.Structures.Model.Chamfer.ChamferTypeEnum.CHAMFER_ARC || cpoint.Chamfer.Type != Tekla.Structures.Model.Chamfer.ChamferTypeEnum.CHAMFER_ARC_POINT))
                {
                    ch.Add(new ark_Chamfer(cpoint.Chamfer.X, cpoint.Chamfer.Y));
                }
            }
            return ch;
        }

        /// <summary>
        /// return true;value if pos not contains "?"
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        Tuple<bool, string> GetPartPos(Part p)
        {
            string POS = "";
            p.GetReportProperty(PART_POS, ref POS);
            if (POS.Contains(@"?")) return new Tuple<bool, string>(false, POS);
            return new Tuple<bool, string>(true, POS);
        }


        void GetMaterial(Part p, ref string material)
        {
            p.GetReportProperty(MATERIAL, ref material);
        }

        void GetLength(Part p, ref double length)
        {
            p.GetReportProperty(LENGTH, ref length);
            length = Math.Round(length, S_value_round);
        }

        void GetProfile(Part p, ref string profile)
        {
            p.GetReportProperty(PROFILE, ref profile);
        }

        /// <summary>
        /// Get info about holes\bolts
        /// double - diam; int - number of holes
        /// </summary>
        /// <param name="p"></param>
        /// <returns>item 1 = have hole or not; item2 - count of holes without checking in intersecting with part</returns>
        //Tuple<bool, int> GetHole(Part p)
        Dictionary<double, int> GetHole(Part p, bool tolerance = true)
        {
            ModelObjectEnumerator moe = p.GetBolts();
            if (moe.GetSize() == 0) return null;

            Dictionary<double, int> value = new Dictionary<double, int>();

            while (moe.MoveNext())
            {
                BoltGroup bg = moe.Current as BoltGroup;
                if (bg.Select())
                {
                    double diam = bg.BoltSize + (tolerance == true ? bg.Tolerance : 0);
                    int count = bg.BoltPositions.Count;
                    if (value.ContainsKey(diam))
                    {
                        value[diam] += count;
                    }
                    else
                    {
                        value.Add(diam, count);
                    }
                }
            }
            return value;
        }

		#region Write to db new items
		void NewItem(string pos, string material, string profile, int num, double length, Dictionary<double, int> holes, bool deform, bool chamf, List<ark_Chamfer> chamfs, List<double> fitt_X, List<double> fitt_Y,bool bools, bool mill)
        {
            table.Add(
                new XElement("item",
	                new XAttribute(ITEM_part_pos.Replace(" ","_"), pos),
	                new XElement(ITEM_profile.Replace(" ", "_"), profile),
	                new XElement(ITEM_material.Replace(" ", "_"), material),
	                new XElement(ITEM_number.Replace(" ", "_"), num),
	                new XElement(ITEM_length.Replace(" ", "_"), length),
	                new XElement(ITEM_part_cutting.Replace(" ", "_"), bools == true ? "+" : "-" ), //bools
	                new XElement(ITEM_bend.Replace(" ", "_"), deform == true ? "+" : "-"),
					new XElement(ITEM_mill.Replace(" ", "_"), mill == true ? "+" : "-")
                ));
            foreach (XElement x in table.Elements("item"))
            {
                if (x.Attribute(ITEM_part_pos.Replace(" ", "_")).Value == pos)
                {
                    // fittings
                    if (fitt_X != null && fitt_X.Count > 0)
                    {
                        for (int i = 0; i < fitt_X.Count; i++)
                        {
                            x.Add(new XElement(ITEM_bevel_X.Replace(" ", "_"), fitt_X[i] + " " + S_degree));
                            x.Add(new XElement(ITEM_bevel_Y.Replace(" ", "_"), fitt_Y[i] + " " + S_degree));
                        }
                    }
                    else
                    {
                        x.Add(new XElement(ITEM_bevel_X.Replace(" ", "_"), ""));
                        x.Add(new XElement(ITEM_bevel_Y.Replace(" ", "_"), ""));
                    }
                    //holes
                    if (holes != null)
                    {
                        foreach (KeyValuePair<double, int> kp in holes)
                        {
                            x.Add(new XElement(ITEM_holes.Replace(" ", "_"), S_diam + kp.Key + ": " + kp.Value + " " + S_number));
                        }
                    }
                    else
                    {
                        x.Add(new XElement(ITEM_holes.Replace(" ", "_"), ""));
                    }
                    // chamfer of end face
                    x.Add(new XElement(ITEM_chamfer_end_face.Replace(" ", "_"), chamf == true ? "+" : "-"));
                    //chamfs of edge
                    if (chamfs.Count > 0)
                    {
                        foreach (ark_Chamfer ch in chamfs)
                        {
                            x.Add(new XElement(ITEM_chamfer_edge.Replace(" ", "_"), "X= " +ch.X +"; Y= " + ch.Y));
                        }
                    }
                    else
                    {
                        x.Add(new XElement(ITEM_chamfer_edge.Replace(" ", "_"), ""));
                    }
                    break;
                }
            }
        }
		#endregion
		#region Update count
        void UpdateNum(string pos)
        {
            foreach(XElement x in table.Elements("item"))
                if (x.Attribute(ITEM_part_pos.Replace(" ", "_")).Value == pos)
                {
                    int i = int.Parse(x.Element(ITEM_number.Replace(" ", "_")).Value);
                    i++;
                    x.Element(ITEM_number.Replace(" ", "_")).Value = i.ToString();
                }
        }
		#endregion
        enum TypePart
        {
            beam,
            polybeam,
            plate,
            unknown
        }

        TypePart tp(Part p)
        {
            if (p is Beam) return TypePart.beam;
            if (p is PolyBeam) return TypePart.polybeam;
            if (p is ContourPlate) return TypePart.plate;
            return TypePart.unknown;
        }


        private void Form1_Load(object sender, EventArgs e)
        {
             LoadConfig();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            XElement cfg = XElement.Load("config.xml");
            string ilang = cfg.Element("language").Attribute("current").Value;
            if (comboBox1.SelectedItem.ToString() != cfg.Element("language").Element(ilang).Value)
            {
                foreach (XElement x in cfg.Elements("language"))
                {
                    foreach (XElement x0 in x.Elements())
                    {
                        if (x0.Value == comboBox1.SelectedItem.ToString())
                        {
                            cfg.Element("language").Attribute("current").Value = x0.Name.LocalName;
                            cfg.Save("config.xml");
                            LoadConfig();
                            return;
                        }
                    }
                }

            }
        }
    }
}
