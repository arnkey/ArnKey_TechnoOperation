﻿namespace ArnKey_TechnoOperation
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_info = new System.Windows.Forms.Button();
            this.deg90 = new System.Windows.Forms.CheckBox();
            this.ixml = new System.Windows.Forms.CheckBox();
            this.open = new System.Windows.Forms.CheckBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.lbl_lang = new System.Windows.Forms.Label();
            this.ch_tol = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_info
            // 
            this.btn_info.Location = new System.Drawing.Point(15, 215);
            this.btn_info.Name = "btn_info";
            this.btn_info.Size = new System.Drawing.Size(260, 23);
            this.btn_info.TabIndex = 0;
            this.btn_info.Text = "Создать отчет";
            this.btn_info.UseVisualStyleBackColor = true;
            this.btn_info.Click += new System.EventHandler(this.button1_Click);
            // 
            // deg90
            // 
            this.deg90.AutoSize = true;
            this.deg90.Checked = true;
            this.deg90.CheckState = System.Windows.Forms.CheckState.Checked;
            this.deg90.Location = new System.Drawing.Point(15, 110);
            this.deg90.Name = "deg90";
            this.deg90.Size = new System.Drawing.Size(220, 17);
            this.deg90.TabIndex = 1;
            this.deg90.Text = "Если скос под углом 90, не учитывать";
            this.deg90.UseVisualStyleBackColor = true;
            // 
            // ixml
            // 
            this.ixml.AutoSize = true;
            this.ixml.Location = new System.Drawing.Point(15, 160);
            this.ixml.Name = "ixml";
            this.ixml.Size = new System.Drawing.Size(95, 17);
            this.ixml.TabIndex = 2;
            this.ixml.Text = "Создать в xml";
            this.ixml.UseVisualStyleBackColor = true;
            // 
            // open
            // 
            this.open.AutoSize = true;
            this.open.Checked = true;
            this.open.CheckState = System.Windows.Forms.CheckState.Checked;
            this.open.Location = new System.Drawing.Point(15, 183);
            this.open.Name = "open";
            this.open.Size = new System.Drawing.Size(70, 17);
            this.open.TabIndex = 3;
            this.open.Text = "Открыть";
            this.open.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(94, 7);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(178, 21);
            this.comboBox1.TabIndex = 4;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // lbl_lang
            // 
            this.lbl_lang.AutoSize = true;
            this.lbl_lang.Location = new System.Drawing.Point(10, 10);
            this.lbl_lang.Name = "lbl_lang";
            this.lbl_lang.Size = new System.Drawing.Size(17, 13);
            this.lbl_lang.TabIndex = 5;
            this.lbl_lang.Text = "lbl";
            // 
            // ch_tol
            // 
            this.ch_tol.AutoSize = true;
            this.ch_tol.Checked = true;
            this.ch_tol.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ch_tol.Location = new System.Drawing.Point(15, 133);
            this.ch_tol.Name = "ch_tol";
            this.ch_tol.Size = new System.Drawing.Size(183, 17);
            this.ch_tol.TabIndex = 6;
            this.ch_tol.Text = "Учитывать зазор в отверстиях";
            this.ch_tol.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Location = new System.Drawing.Point(12, 34);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(263, 70);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(85, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "radioButton1";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(6, 42);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(85, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "radioButton2";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 250);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ch_tol);
            this.Controls.Add(this.lbl_lang);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.open);
            this.Controls.Add(this.ixml);
            this.Controls.Add(this.deg90);
            this.Controls.Add(this.btn_info);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ArnKey\'s Techno Operations";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_info;
        private System.Windows.Forms.CheckBox deg90;
        private System.Windows.Forms.CheckBox ixml;
        private System.Windows.Forms.CheckBox open;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label lbl_lang;
        private System.Windows.Forms.CheckBox ch_tol;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
    }
}

