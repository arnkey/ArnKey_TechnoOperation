﻿using System;
using System.Windows.Forms;

namespace ArnKey_TechnoOperation
{
	public class Settings : Form
	{

		private DataGridView data = new DataGridView ();
		private Button btn_Ok = new Button ();

		public Settings ()
		{

			this.Width = 450;
			this.Height = 450;

			this.FormBorderStyle = FormBorderStyle.FixedDialog;
			this.StartPosition = FormStartPosition.CenterScreen;
			this.TopMost = true;

			data.Top = 0;
			data.Left = 0;
			data.Height = 375;
			data.Width = this.Width;

			data.Columns.Add ("profile", "Профиль");
			data.Columns.Add ("length", "Габарит");

			btn_Ok.Text = "Ok";
			btn_Ok.DialogResult = DialogResult.OK;
			btn_Ok.Width = 100;
			btn_Ok.Left = (int)(this.Width * 0.5 - btn_Ok.Width * 0.5);
			btn_Ok.Top = this.Height - btn_Ok.Height * 2 - 15;

			this.Controls.Add (btn_Ok);
			this.Controls.Add (data);
		}
	}
}
